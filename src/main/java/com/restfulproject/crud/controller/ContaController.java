package com.restfulproject.crud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restfulproject.crud.entities.Conta;
import com.restfulproject.crud.service.ContaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/conta")
@Api(value = "conta", description = "Manipular a conta controller", tags = {"Conta"})
public class ContaController {
	
	@Autowired
	public ContaService contaService;
	
	@ApiOperation(value="Listar todas as contas", produces = "application/json")
	@RequestMapping(value="/findAll",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> findAll(){
		return new ResponseEntity<>(contaService.listarContas(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Deletar Titular e seus Adicionais pelo id da conta")
	@RequestMapping(value="/deletarTitularPeloIdConta/{idConta}",method=RequestMethod.DELETE, produces=MediaType.ALL_VALUE)
	public ResponseEntity<?> deletarTitularPeloIdConta(@PathVariable int idConta){
		
		if(!contaService.verificaContaExistente(idConta))
			return new ResponseEntity<>("Conta inexistente", HttpStatus.OK);
		
		String response = contaService.deletarContaTitularEAdicionais(idConta);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Listar Conta pelo ID")
	@RequestMapping(value = "/findById/{idConta}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Conta findById(int idConta) {
		
		return contaService.findById(idConta);
		
	}

}
