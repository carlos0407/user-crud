package com.restfulproject.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restfulproject.crud.dto.ContactDto;
import com.restfulproject.crud.dto.RelationDto;
import com.restfulproject.crud.entities.Conta;
import com.restfulproject.crud.entities.ContaPagamento;
import com.restfulproject.crud.entities.User;
import com.restfulproject.crud.entities.UserContact;
import com.restfulproject.crud.exceptions.InvalidRequestException;
import com.restfulproject.crud.exceptions.MissingFieldsFilledException;
import com.restfulproject.crud.responses.ContaPagamentoResponse;
import com.restfulproject.crud.service.ContaPagamentoService;
import com.restfulproject.crud.service.ContaService;
import com.restfulproject.crud.service.ContactService;
import com.restfulproject.crud.service.UserContactService;
import com.restfulproject.crud.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/contaPagamento")
@Api(value = "contaPagamento", description = "Manipular a ContaPagamento Controller", tags = {"ContaPagamento"})
public class ContaPagamentoController {
	
	@Autowired
	public ContaService contaService;
	
	@Autowired
	public UserService userService;
	
	@Autowired
	public ContactService contactService;
	
	@Autowired
	public UserContactService userContactService;
	
	@Autowired
	public ContaPagamentoService contaPagamentoService;
	
	@ApiOperation(value = "Adicionar conta titular pelo ID do Usuaŕio")
	@RequestMapping(value = "/addContaTitular/{idUser}",method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> addContaTitular(@PathVariable int idUser) {
		
		ContaPagamento contaPagamento = contaPagamentoService.salvarContaTitular(idUser);
		
		if(contaPagamento == null) {
			throw new MissingFieldsFilledException("Conta Titular já cadastrada para este usuário!");
		}
		
		User user = userService.buscarUsuario(idUser);
		Conta conta = contaService.findById(contaPagamento.getIdConta());
		
		ContaPagamentoResponse response = new ContaPagamentoResponse();
		
		response.setUser(user);
		response.setConta(conta);
		response.setTitularidade(contaPagamento.getTitularidade());
		
		
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Adicionar conta adicional pelo ID da conta")
	@RequestMapping(value = "/addContaAdicional/{idConta}", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> addContaAdicional(@RequestBody RelationDto userContactDto, @PathVariable int idConta, BindingResult result){
		
		if(userContactDto.getContacts().size() > 3) {
			throw new InvalidRequestException("Limite de 3 contatos por usuário", result);
		}
		
		if(!contactService.verifyPhone(userContactDto.getContacts(), 0)) {
			throw new InvalidRequestException("Telefone Inválido", result);
		}
		
		if(!userService.validaCpf(userContactDto.getCpf()) || !userService.verifyEqualCpf(userContactDto.getCpf())) {
			throw new InvalidRequestException("CPF inválido!", result);
		}
		
		User user = userService.salvarUsuario(userContactDto);
		List<ContactDto> contacts = contactService.salvarContatos(userContactDto);
		
		int valor = contacts.size();
		
		for(int i=0; i<valor; i++) {
			userContactDto.setIdUser(user.getId());
			userContactDto.setContact(contacts.get(i));
			
			@SuppressWarnings("unused")
			UserContact userContact = userContactService.salvar(user.getId(), contacts.get(i).getIdContact());
		}
		
		ContaPagamento contaPagamento = contaPagamentoService.salvarContaAdicional(idConta, user.getId());
		
		ContaPagamentoResponse response = new ContaPagamentoResponse();
		
		response.setConta(contaService.findById(contaPagamento.getIdConta()));
		response.setTitularidade(contaPagamento.getTitularidade());
		response.setUser(user);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Deletar conta adicional pelo ID do usuário")
	@RequestMapping(value = "/deletarContaAdicionalPeloIdUser/{idUser}", method=RequestMethod.DELETE, produces = MediaType.ALL_VALUE)
	public ResponseEntity<?> deletarContaAdicionalPeloIdUser(@PathVariable int idUser) {
		
		if(!contaPagamentoService.verificaIdUserTitularidade(idUser))
			return new ResponseEntity<>("Usuário não encontrado", HttpStatus.OK);
		
		String msg = contaPagamentoService.deletarContaAdicional(idUser);
		
		return new ResponseEntity<>(msg, HttpStatus.OK);
	}
	
	
	
	
	
}
