package com.restfulproject.crud.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restfulproject.crud.dto.ContactDto;
import com.restfulproject.crud.entities.Contact;
import com.restfulproject.crud.entities.User;
import com.restfulproject.crud.entities.UserContact;
import com.restfulproject.crud.exceptions.InvalidRequestException;
import com.restfulproject.crud.service.ContactService;
import com.restfulproject.crud.service.UserContactService;
import com.restfulproject.crud.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@RestController
@RequestMapping("/api/contact")
@Api(value = "contact", description= "Manipular a Contact Controller", tags = {"Contact"})
public class ContactController {
	
	@Autowired
	public UserService userService;
	
	@Autowired
	public ContactService contactService;
	
	@Autowired
	public UserContactService userContactService;
	
	// MÉTODO PARA ADICIONAR CONTATO PELO ID DO USUÁRIO
	@SuppressWarnings("unused")
	@ApiOperation(value = "Adicionar Contato(s) pelo id do Usuário")
	@RequestMapping(value = "/addContactByUserId/{idUser}", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> addContactByUserId(@RequestBody List<ContactDto> contactsDto, @PathVariable Integer idUser, BindingResult result){
		
		if(contactService.buscarContatosPorIdUsuario(idUser).size() == 3 || (contactService.buscarContatosPorIdUsuario(idUser).size() + contactsDto.size()) > 3) {
			throw new InvalidRequestException("Limite de 3 contatos por usuário", result);
		}
		
		if(!contactService.verifyPhone(contactsDto, idUser)) {
			throw new InvalidRequestException("Telefone ou DDD Inválidos", result);
		}
		
		List<ContactDto> contacts = contactService.salvarContatoPorIdUsuario(contactsDto);
		
		
		User user = userService.buscarUsuario(idUser);
		
		
		for (ContactDto contactDto : contacts) {
			UserContact userContact = new UserContact();
			userContact = userContactService.salvar(idUser, contactDto.getIdContact());
		}
		
		//UserContact userContact = userContactService.salvar(relationDto, contacts.getIdContact());
		
		HashMap<String, Object> map = new HashMap<>();
		
		if(result.hasErrors()) {
			map.put("msg", "Dados Inválidos");
			return new ResponseEntity<>(map, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(contacts, HttpStatus.OK);
	}
	
	//MÉTODO PARA DELETAR CONTATO PELO ID
	@ApiOperation(value = "Deletar o contato pelo ID")
	@RequestMapping(value = "/deleteContactById/{idContact}", method=RequestMethod.DELETE, produces=MediaType.ALL_VALUE)
	public ResponseEntity<?> deleteContactById(@PathVariable Integer idContact){
		
		String message = contactService.deletarContato(idContact);
		
		if(message == null) {
			return new ResponseEntity<>("Contato Inexistente", HttpStatus.OK);
		}
		
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	
	//MÉTODO PARA ATUALIZAR CONTATO PELO ID
	@ApiOperation(value = "Modificar o contato pelo ID")
	@RequestMapping(value = "/updateContactById/{idContact}", method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> updateContactById(@RequestBody ContactDto contactDto, @PathVariable Integer idContact, BindingResult result){
		
		if(result.hasErrors()) {
			throw new HttpMessageNotReadableException("Telefone ou ddd inválidos");
		}
		
		if(!contactService.validatePhone(contactDto.getPhone()) || !contactService.validateDdd(contactDto.getDdd())) {
			throw new InvalidRequestException("Telefone ou ddd inválido!", result);
		}
		
		Contact contact = contactService.atualizarContatoPorId(idContact, contactDto);
		
		HashMap<String, Object> map = new HashMap<>();
		
		if(result.hasErrors()) {
			map.put("msg", "Dados Inválidos");
			return new ResponseEntity<>(map, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(contact, HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Listar todos os contatos")
	@RequestMapping(value = "/listarContatos", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> listarContatos(){
		
		return new ResponseEntity<>(contactService.listarContatos(), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Listar contato pelo ID")
	@RequestMapping(value="/listarContatoPeloId", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> listarContatoPeloId(int idContact){
		
		return new ResponseEntity<>(contactService.buscarContato(idContact), HttpStatus.OK);
		
	}
	
	
	
	

}
