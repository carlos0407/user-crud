package com.restfulproject.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restfulproject.crud.responses.ResponseUserContactConta;
import com.restfulproject.crud.service.ContaPagamentoService;
import com.restfulproject.crud.service.ContaService;
import com.restfulproject.crud.service.ContactService;
import com.restfulproject.crud.service.UserContactService;
import com.restfulproject.crud.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/UserContactConta")
@Api(value = "userContactConta", description = "Manipular a UserContactConta controller", tags = {"UserContactConta"})
public class UserContactContaController {
	
	@Autowired
	public ContaService contaService;
	
	@Autowired
	public UserService userService;
	
	@Autowired
	public ContactService contactService;
	
	@Autowired
	public UserContactService userContactService;
	
	@Autowired
	public ContaPagamentoService contaPagamentoService;
	
	// MÉTODO PARA DELETAR USUÁRIO E SEUS RESPECTIVOS CONTATOS PELO ID DO USUÁRIO
	@ApiOperation(value = "Deletar o Usuário - Seus contatos, suas contas e seus adicionais")
	@RequestMapping(value = "/deleteUserContactContaByIdUser/{idUser}", method=RequestMethod.DELETE, produces=MediaType.ALL_VALUE)
	public ResponseEntity<?> deleteUserContactContaByIdUser(@PathVariable Integer idUser){
		
		if(!userService.verificaUsuarioExistente(idUser))
			return new ResponseEntity<>("Usuário Inexistente", HttpStatus.OK);
		
		String message = userContactService.deleteUserContactsConta(idUser);
		
		return new ResponseEntity<>(message, HttpStatus.OK);
		 
	}
	
	@ApiOperation(value = "Listar Usuários com seus contatos, suas contas e seus adiconais")
	@RequestMapping(value = "/listarTudo", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> listarTudo(){
		List<ResponseUserContactConta> response = contaPagamentoService.findAll();
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Listar Usuário com seus contatos, sua conta e seus adicionais pelo seu ID")
	@RequestMapping(value = "/listarPeloIdUser/{idUser}", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> listarPeloId(@PathVariable int idUser){
		
		ResponseUserContactConta response = contaPagamentoService.findById(idUser);
		
		if(response == null)
			return new ResponseEntity<>("Apenas Usuários com Contas Titulares", HttpStatus.OK);
		else
			return new ResponseEntity<>(response, HttpStatus.OK);
		
	} 
	
	
}
