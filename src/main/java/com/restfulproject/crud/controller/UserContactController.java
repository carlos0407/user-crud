package com.restfulproject.crud.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restfulproject.crud.dto.ContactDto;
import com.restfulproject.crud.dto.RelationDto;
import com.restfulproject.crud.entities.User;
import com.restfulproject.crud.entities.UserContact;
import com.restfulproject.crud.exceptions.InvalidRequestException;
import com.restfulproject.crud.responses.ResponseUserContact;
import com.restfulproject.crud.service.ContactService;
import com.restfulproject.crud.service.UserContactService;
import com.restfulproject.crud.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/userContact")
@Api(value = "userContact", description = "Manipular a UserContactController", tags = {"UserContact"})
public class UserContactController {

	@Autowired
	public UserService userService;
	
	@Autowired
	public ContactService contactService;
	
	@Autowired
	public UserContactService userContactService;
	
	// MÉTODO PARA ADICIONAR USUÁRIO E CONTATO
	@SuppressWarnings("unused")
	@ApiOperation(value = "Adicionar Usuário e seus Contatos")
	@RequestMapping(value = "/add", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> add(@RequestBody @Valid RelationDto userContactDto, BindingResult result){
		
		if(userContactDto.getContacts().size() > 3) {
			throw new InvalidRequestException("Limite de 3 contatos por usuário", result);
		}
		
		if(!contactService.verifyPhone(userContactDto.getContacts(), 0)) {
			throw new InvalidRequestException("Telefone Inválido", result);
		}
		
		if(!userService.validaCpf(userContactDto.getCpf()) || !userService.verifyEqualCpf(userContactDto.getCpf())) {
			throw new InvalidRequestException("CPF inválido!", result);
		}
		
		User user = userService.salvarUsuario(userContactDto);
		List<ContactDto> contacts = contactService.salvarContatos(userContactDto);
		
		int valor = contacts.size();
		
		for(int i=0; i<valor; i++) {
			userContactDto.setIdUser(user.getId());
			userContactDto.setContact(contacts.get(i));
			
			UserContact userContact = userContactService.salvar(user.getId(), contacts.get(i).getIdContact());
		}
		
		ResponseUserContact response = userContactService.findUserContactById(user.getId());
		
		HashMap<String, Object> map = new HashMap<>();
		
		if(result.hasErrors()) {
			map.put("msg", "Dados inválidos");
			return new ResponseEntity<>(map, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	
	}
	
	
	// MÉTODO PARA ACHAR TODOS OS USUARIOS COM SEUS RESPECTIVOS CONTATOS
	@ApiOperation(value = "Encontrar todos os usuários e seus contatos")
	@RequestMapping(value = "/findAll", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> findAll(){
		List<ResponseUserContact> userContacts= userContactService.encontrar();
				
		return new ResponseEntity<>(userContacts, HttpStatus.OK);
	}
	
	// MÉTODO PARA ACHAR UM USUÁRIO COM SEUS RESPECTIVOS CONTATOS PELO ID DO USUÁRIO
	@ApiOperation(value = "Encontrar Usuário e seus contatos pelo ID do Usuário")
	@RequestMapping(value = "/findUserContactByIdUser/{idUser}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> findUserContactByIdUser(@PathVariable int idUser){
		ResponseUserContact userContacts= userContactService.findUserContactById(idUser);
				
		return new ResponseEntity<>(userContacts, HttpStatus.OK);
	}

}
