package com.restfulproject.crud.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restfulproject.crud.dto.UserDto;
import com.restfulproject.crud.entities.User;
import com.restfulproject.crud.exceptions.InvalidRequestException;
import com.restfulproject.crud.service.ContactService;
import com.restfulproject.crud.service.UserContactService;
import com.restfulproject.crud.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/user")
@Api(value = "user", description = "Manipular a UserController", tags = {"User"})
public class UserController {
	
	@Autowired
	public UserService userService;
	
	@Autowired
	public ContactService contactService;
	
	@Autowired
	public UserContactService userContactService;
	
	// MÉTODO PARA ATUALIZAR UM USUÁRIO PELO ID DELE
	@ApiOperation(value = "Atualizar Usuário pelo ID")
	@RequestMapping(value = "/updateUserById/{idUser}", method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> updateUserById(@RequestBody UserDto userDto, @PathVariable int idUser, BindingResult result){
		
		if(!userService.validaCpf(userDto.getCpf())) {
			throw new InvalidRequestException("CPF inválido!", result);
		}
		
		User user = userService.alterarUsuarioPorId(idUser, userDto);
		
		HashMap<String, Object> map = new HashMap<>();
		
		if(result.hasErrors()) {
			map.put("msg", "Dados Inválidos");
			return new ResponseEntity<>(map, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(user, HttpStatus.OK);
		
	}
	
	
	
	

}














