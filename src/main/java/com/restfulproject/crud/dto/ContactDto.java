package com.restfulproject.crud.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Contact")
public class ContactDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int idContact;
	private int ddd;
	private long phone;
	
	@ApiModelProperty(hidden = true)
	public int getIdContact() {
		return idContact;
	}
	public void setIdContact(int idContact) {
		this.idContact = idContact;
	}
	@ApiModelProperty(position = 1, allowEmptyValue = false)
	public int getDdd() {
		return ddd;
	}
	public void setDdd(int ddd) {
		this.ddd = ddd;
	}
	@ApiModelProperty(position = 2, allowEmptyValue = false)
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	

}
