package com.restfulproject.crud.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Relation")
public class RelationDto implements Serializable{
	private static final long serialVersionUID = 1L;

	
	private int idUser;
	private String firstname;
	private String lastname;
	private String address;
	private String cpf;
	
	private List<ContactDto> contacts;
	
	
	@ApiModelProperty(hidden = true)
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	@ApiModelProperty(position = 1, allowEmptyValue = false)
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	@ApiModelProperty(position = 2, allowEmptyValue = false)
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	@ApiModelProperty(position = 3, allowEmptyValue = false)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@ApiModelProperty(position = 4, allowEmptyValue = false)
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	@ApiModelProperty(position = 5, allowEmptyValue = false)
	public List<ContactDto> getContacts() {
		return contacts;
	}
	public void setContacts(List<ContactDto> contacts) {
		this.contacts = contacts;
	}
	@ApiModelProperty(hidden = true)
	public void setContact(ContactDto contact) {
		this.contacts.add(contact);
	}
	
	
	
	
	
}