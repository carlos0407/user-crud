package com.restfulproject.crud.dto;

import java.io.Serializable;

public class UserContactDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int idUser;
	private int idContact;
	
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdContact() {
		return idContact;
	}
	public void setIdContact(int idContact) {
		this.idContact = idContact;
	}
	
	

}
