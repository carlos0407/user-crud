package com.restfulproject.crud.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "\"conta\"")
public class Conta implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@SequenceGenerator(name="conta_id_seq", sequenceName="conta_id_seq")
	private Integer id;
	
	@Column(name="conta_pagamento", nullable=false)
	private long contaPagamento;

	public Conta() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Conta(Integer id, long contaPagamento) {
		super();
		this.id = id;
		this.contaPagamento = contaPagamento;
	}

	@ApiModelProperty(position = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ApiModelProperty(position = 2)
	public long getContaPagamento() {
		return contaPagamento;
	}

	public void setContaPagamento(long contaPagamento) {
		this.contaPagamento = contaPagamento;
	}
}
