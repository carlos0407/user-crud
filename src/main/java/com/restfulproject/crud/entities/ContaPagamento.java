package com.restfulproject.crud.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "\"conta_pagamento\"")
@IdClass(ContaPagamentoId.class)
public class ContaPagamento {

	@Id
	@Column(name="id_usuario")
	private int userId;
	@Id
	@Column(name="id_conta")
	private int contaId;
	
	private int titularidade;

	public int getIdUser() {
		return userId;
	}

	public void setIdUser(int idUser) {
		this.userId = idUser;
	}

	public int getIdConta() {
		return contaId;
	}

	public void setIdConta(int idConta) {
		this.contaId = idConta;
	}

	public int getTitularidade() {
		return titularidade;
	}

	public void setTitularidade(int titularidade) {
		this.titularidade = titularidade;
	}

	public ContaPagamento() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ContaPagamento(int idUser, int idConta, int titularidade) {
		super();
		this.userId = idUser;
		this.contaId = idConta;
		this.titularidade = titularidade;
	}

	
	
	
	
	
}
