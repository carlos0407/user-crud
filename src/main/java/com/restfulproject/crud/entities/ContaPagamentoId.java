package com.restfulproject.crud.entities;

import java.io.Serializable;

public class ContaPagamentoId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	private int contaId;
	private int userId;
	
	public ContaPagamentoId() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ContaPagamentoId(int contaId, int userId) {
		super();
		this.contaId = contaId;
		this.userId = userId;
	}

	public int getContaId() {
		return contaId;
	}

	public int getUserId() {
		return userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + contaId;
		result = prime * result + userId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaPagamentoId other = (ContaPagamentoId) obj;
		if (contaId != other.contaId)
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
	
	
	
	
	

}
