package com.restfulproject.crud.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="\"contacts\"")
public class Contact implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@SequenceGenerator(name="contacts_id_seq", sequenceName="contacts_id_seq")
	private int id;
	
	@Column(name="ddd", nullable=false)
	private Integer ddd;
	@Column(name="phone", nullable=false)
	private Long phone;
	
	

	public Contact() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Contact(Integer ddd, Long phone) {
		super();
		this.ddd = ddd;
		this.phone = phone;
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getDdd() {
		return ddd;
	}
	public void setDdd(Integer ddd) {
		this.ddd = ddd;
	}
	public Long getPhone() {
		return phone;
	}
	public void setPhone(Long phone) {
		this.phone = phone;
	}
}
