package com.restfulproject.crud.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="\"users\"")
public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4986635240092422790L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@SequenceGenerator(name="users_id_seq", sequenceName="users_id_seq")
	private int id;
	
	@Column(name = "firstname", nullable = false)
	@Size(min=1, message = "Nome é obrigatório")
	private String firstname;
	@Column(name = "lastname", nullable = false)
	@Size(min = 1, message = "Sobrenome é obrigatório")
	private String lastname;
	@Column(name = "address", nullable = false)
	private String address;
	@Column(name = "cpf", nullable = false)
	private String cpf;
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public User(String firstname, String lastname, String address, String cpf) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.cpf = cpf;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
}