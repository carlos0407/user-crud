package com.restfulproject.crud.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "\"user_contacts\"")
@IdClass(value = UserContactId.class)
public class UserContact implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "user_id")
	private Integer userId;
	@Id
	@Column(name = "contact_id")
	private Integer contactId;
	
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getContactId() {
		return contactId;
	}
	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
	
	public UserContact() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserContact(Integer userId, Integer contactId) {
		super();
		this.userId = userId;
		this.contactId = contactId;
	}
}
