package com.restfulproject.crud.entities;

import java.io.Serializable;

public class UserContactId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int userId;
	private int contactId;
	
	public UserContactId() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserContactId(int userId, int contactId) {
		super();
		this.userId = userId;
		this.contactId = contactId;
	}

	public int getUserId() {
		return userId;
	}

	public int getContactId() {
		return contactId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + contactId;
		result = prime * result + userId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserContactId other = (UserContactId) obj;
		if (contactId != other.contactId)
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
	
	
	
	
	
	

}
