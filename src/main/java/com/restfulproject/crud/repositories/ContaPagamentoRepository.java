package com.restfulproject.crud.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restfulproject.crud.entities.ContaPagamento;
import com.restfulproject.crud.entities.ContaPagamentoId;

public interface ContaPagamentoRepository extends JpaRepository<ContaPagamento, ContaPagamentoId>{
	
}
