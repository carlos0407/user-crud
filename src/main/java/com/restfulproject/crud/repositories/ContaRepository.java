package com.restfulproject.crud.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.restfulproject.crud.entities.Conta;

public interface ContaRepository extends JpaRepository<Conta, Integer>{
	
	@Query(value="select nextval('conta_pag_seq')",nativeQuery=true)
	int getSequence();
	
}
