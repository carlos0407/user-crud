package com.restfulproject.crud.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restfulproject.crud.entities.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer>{
	
	Contact findById(int idContact);
	
	Contact findByPhone(Long phone);

}
