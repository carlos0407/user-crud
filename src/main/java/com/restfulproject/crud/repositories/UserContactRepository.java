package com.restfulproject.crud.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restfulproject.crud.entities.UserContact;
import com.restfulproject.crud.entities.UserContactId;


public interface UserContactRepository extends JpaRepository<UserContact, UserContactId>{
	
	
	
}
