package com.restfulproject.crud.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restfulproject.crud.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	User findById(int idUser);
	
	User findByCpf(String cpf);
	
}
