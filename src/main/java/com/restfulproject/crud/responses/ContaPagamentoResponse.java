package com.restfulproject.crud.responses;

import com.restfulproject.crud.entities.Conta;
import com.restfulproject.crud.entities.User;

public class ContaPagamentoResponse {
	
	private User user;
	private Conta conta;
	private String titularidade;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public String getTitularidade() {
		return titularidade;
	}
	public void setTitularidade(int titularidade) {
		if(titularidade == 1) {
			this.titularidade = "Titular";
		}
		if(titularidade == 2) {
			this.titularidade = "Adicional";
		}
	}
	
	public ContaPagamentoResponse() {
		
	}
	
	

}
