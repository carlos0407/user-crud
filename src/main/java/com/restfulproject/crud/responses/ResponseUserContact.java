package com.restfulproject.crud.responses;

import java.util.List;

import com.restfulproject.crud.entities.Contact;
import com.restfulproject.crud.entities.User;

public class ResponseUserContact {
	
	private User user;
	private List<Contact> contacts;
	
	
	
	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public List<Contact> getContacts() {
		return contacts;
	}



	public void setContact(List<Contact> contacts) {
		this.contacts = contacts;
	}



	public ResponseUserContact() {
		
	}
	
	

}
