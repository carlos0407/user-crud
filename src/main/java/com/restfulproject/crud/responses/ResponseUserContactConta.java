package com.restfulproject.crud.responses;

import java.util.List;

import com.restfulproject.crud.entities.Conta;
import com.restfulproject.crud.entities.Contact;
import com.restfulproject.crud.entities.User;

public class ResponseUserContactConta{
	
	private User user;
	private List<Contact> contacts;
	private Conta conta;
	private List<ResponseUserContact> adicionais;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Contact> getContacts() {
		return contacts;
	}
	public void setContact(List<Contact> contacts) {
		this.contacts = contacts;
	}
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public List<ResponseUserContact> getAdicionais() {
		return adicionais;
	}
	public void setAdicionais(List<ResponseUserContact> adicionais) {
		this.adicionais = adicionais;
	}
	
	public ResponseUserContactConta() {
		
	}

	
	
	

}
