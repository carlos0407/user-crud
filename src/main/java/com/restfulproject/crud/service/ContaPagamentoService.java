package com.restfulproject.crud.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restfulproject.crud.entities.Conta;
import com.restfulproject.crud.entities.ContaPagamento;
import com.restfulproject.crud.repositories.ContaPagamentoRepository;
import com.restfulproject.crud.responses.ResponseUserContact;
import com.restfulproject.crud.responses.ResponseUserContactConta;

@Service
@Transactional
public class ContaPagamentoService {
	
	private final int TITULAR = 1;
	private final int ADICIONAL = 2;
	
	@Autowired
	private ContaPagamentoRepository contaPagamentoRepository;
	
	@Autowired
	private ContaService contaService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserContactService userContactService;
	
	
	public List<ContaPagamento> listarContaPagamento(){
		return contaPagamentoRepository.findAll();
	}
	
	public ContaPagamento salvarContaTitular(int idUser) {
		
		ContaPagamento contaPagamento = new ContaPagamento();
		
		List<ContaPagamento> contasPagamento = listarContaPagamento();
		
		
		if(!userService.verificaUsuarioExistente(idUser))
			return null;
		
		for(ContaPagamento conta: contasPagamento) {
			if(conta.getIdUser() == idUser)
				return null;
		}
		
		Conta conta = contaService.salvarConta(idUser);
		
		
		
		contaPagamento.setIdConta(conta.getId());
		contaPagamento.setIdUser(idUser);
		contaPagamento.setTitularidade(TITULAR);
		
		contaPagamentoRepository.save(contaPagamento);
		
			
		return contaPagamento;
	}
	
	public ContaPagamento salvarContaAdicional(int idConta, int idUser) {
		
		ContaPagamento contaPagamento = new ContaPagamento();
		
		if(!contaService.verificaContaExistente(idConta))
			return null;
		
		contaPagamento.setIdConta(idConta);
		contaPagamento.setIdUser(idUser);
		contaPagamento.setTitularidade(ADICIONAL);
		
		contaPagamentoRepository.save(contaPagamento);
		
		return contaPagamento;
		
	}
	
	public String deletarContaAdicional(int idUser) {
		if(!userService.verificaUsuarioExistente(idUser)) {
			return "Usuário Inexistente";
		}
		List<ContaPagamento> contasPagamento = listarContaPagamento();
		String nome = new String();
		for(ContaPagamento conta: contasPagamento) {
			if(conta.getIdUser() == idUser) {
				if(conta.getTitularidade() == 1) {
					return "Impossível deletar conta do usuário titular";
				} else {
					nome = userService.buscarUsuario(idUser).getFirstname(); 
					contaPagamentoRepository.delete(conta);
					break;
				}
			}
		}
		
		userContactService.deleteUserContacts(idUser);
		
		
		return "Conta do "+nome+" deletada com sucesso";
	}
	
	public void deletarContaPagamento(ContaPagamento contaPagamento) {
		contaPagamentoRepository.delete(contaPagamento);
	}
	
	public boolean verificaIdUserTitularidade(int idUser) {
		List<ContaPagamento> contas = listarContaPagamento();
		
		for(ContaPagamento conta: contas) {
			if(conta.getIdUser() == idUser) {
					return true;
			}
		}
	
	return false;

	}
	
	public boolean verificaContaExistenteIdUser(int idUser) {
		List<ContaPagamento> contas = listarContaPagamento();
		
		for(ContaPagamento conta: contas) {
			if(conta.getIdUser() == idUser)
				return true;
		}
		
		return false; //Conta não existente nesse usuário
	}
	
	public int verificaContaAdicionalOuTitular(int idUser) {
		
		List<ContaPagamento> contas = listarContaPagamento();
		
		for(ContaPagamento conta: contas)
			if(conta.getIdUser() == idUser)
				return conta.getTitularidade();
		
		return 0; //Este return é apenas para tirar o erro de return
	}
	
	public ContaPagamento findContaPagamentoByIdUser(List<ContaPagamento> contas, int idUser) {
		
		for(ContaPagamento conta: contas) {
			if(idUser == conta.getIdUser())
				return conta;
		}
		
		return null;//Este return é apenas para tirar o erro de return 

		
	}
	
	public int findIdContaPeloIdUser(List<ContaPagamento> contas, int idUser) {
		
		for(int i = 0; i < contas.size(); i++) {
			if(idUser == contas.get(i).getIdUser()) {
				return contas.get(i).getIdConta();
			}
		}
		return 0;//Este return é apenas para tirar o erro de return 
	}
	
	public List<ResponseUserContactConta> findAll() {
		
		List<ResponseUserContact> responseUserContact = userContactService.encontrar();
		
		List<ContaPagamento> contas = listarContaPagamento();
		
		List<ResponseUserContactConta> response = new ArrayList<ResponseUserContactConta>();
		
		for(ContaPagamento conta: contas) {
			int idConta = conta.getIdConta();
			
			ResponseUserContactConta responseUserContactConta = new ResponseUserContactConta();
			List<ResponseUserContact> usersContacts = new ArrayList<ResponseUserContact>();
			
			if(conta.getTitularidade() == 1) {
				
				for(ResponseUserContact responseUC: responseUserContact) {
					
					if(responseUC.getUser().getId() == conta.getIdUser()) {
						responseUserContactConta.setUser(responseUC.getUser());
						responseUserContactConta.setContact(responseUC.getContacts());
						responseUserContactConta.setConta(contaService.findById(conta.getIdConta()));
					}
					for(ContaPagamento conta2: contas) {
						if(conta2.getTitularidade() == 2 && conta2.getIdConta() == idConta && conta2.getIdUser() == responseUC.getUser().getId()) {
							usersContacts.add(responseUC);
						}
					}
					
					responseUserContactConta.setAdicionais(usersContacts);
				}
				
				response.add(responseUserContactConta);
				
			} else {
				continue;
			}
			
		}
		
		for(ResponseUserContact userContact: responseUserContact) {
			if(!verificaContaExistenteIdUser(userContact.getUser().getId())) {
				ResponseUserContact responseUC = userContactService.findUserContactById(userContact.getUser().getId());
				
				ResponseUserContactConta responseUserContactConta = new ResponseUserContactConta();
				
				responseUserContactConta.setUser(responseUC.getUser());
				responseUserContactConta.setContact(responseUC.getContacts());
				
				response.add(responseUserContactConta);
			}
		}
		
		return response;			
	}
	
	public ResponseUserContactConta findById(int idUser) {
		
		if(!verificaContaExistenteIdUser(idUser)) {
			ResponseUserContact responseUC = userContactService.findUserContactById(idUser);
			
			ResponseUserContactConta response = new ResponseUserContactConta();
			
			response.setUser(responseUC.getUser());
			response.setContact(responseUC.getContacts());
			
			return response;
		}
		
		if(verificaContaAdicionalOuTitular(idUser) == ADICIONAL)
			return null;
		
		List<ResponseUserContact> responseUserContact = userContactService.encontrar();
		
		List<ContaPagamento> contas = listarContaPagamento();
		
		ResponseUserContactConta response = new ResponseUserContactConta();
		
		for(ContaPagamento conta: contas) {
			int idConta = conta.getIdConta();
			
			List<ResponseUserContact> usersContacts = new ArrayList<ResponseUserContact>();
			
			if(conta.getTitularidade() == 1) { // 1 = Titular
				
				for(ResponseUserContact responseUC: responseUserContact) {
					
					if(responseUC.getUser().getId() == idUser && conta.getIdUser() == idUser) {
						response.setUser(responseUC.getUser());
						response.setContact(responseUC.getContacts());
						response.setConta(contaService.findById(conta.getIdConta()));
					}
					for(ContaPagamento conta2: contas) {
						if(conta2.getTitularidade() == 2 && conta2.getIdConta() == idConta && conta2.getIdUser() == responseUC.getUser().getId()) {
							usersContacts.add(responseUC);
						}
					}
					
				}
				
				response.setAdicionais(usersContacts);
				
			} else {
				continue;
			}
			

			
		}
		
		return response;
		
	}
	
	
}
