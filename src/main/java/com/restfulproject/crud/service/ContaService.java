package com.restfulproject.crud.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restfulproject.crud.entities.Conta;
import com.restfulproject.crud.entities.ContaPagamento;
import com.restfulproject.crud.repositories.ContaRepository;

@Service
@Transactional
public class ContaService {
	
	@Autowired
	private ContaRepository contaRepository;
	
	@Autowired
	private ContaPagamentoService contaPagamentoService;
	
	public List<Conta> listarContas(){
		return contaRepository.findAll();
	}
	
	public Conta findById(int idConta) {
		return contaRepository.findOne(idConta);
	}
	
	public String deleteConta(int idConta) {
		
		contaRepository.delete(idConta);
		return "Deletado com sucesso";
	}
	
	public Conta salvarConta(int idUser) {
		Conta conta = new Conta();
		
		conta.setContaPagamento(contaRepository.getSequence());
		
		contaRepository.save(conta);
		
		String contaPagamento = Integer.toString(idUser);
		
		//
		contaPagamento = StringUtils.rightPad(contaPagamento, 4, '0');
		contaPagamento += StringUtils.leftPad(Integer.toString(conta.getId()), 4, '0');
		contaPagamento += StringUtils.leftPad(Long.toString(conta.getContaPagamento()), 4, '0');
		
		conta.setContaPagamento(Long.parseLong(contaPagamento));
		
		contaRepository.save(conta);
		
		return conta;
	}
	
	public String deletarContaTitularEAdicionais(int idConta) {
		
		if(idConta == -1)
			return null;
		
		List<ContaPagamento> contasPagamento = contaPagamentoService.listarContaPagamento();
		
		List<Conta> contas = listarContas();
		
		for(ContaPagamento contaPagamento: contasPagamento) {
			if(contaPagamento.getIdConta() == idConta) {
				if(contaPagamento.getTitularidade() == 1) {
					contaPagamentoService.deletarContaPagamento(contaPagamento);
				} else {
					contaPagamentoService.deletarContaAdicional(contaPagamento.getIdUser());
				}
			}
		}
		
		for(Conta conta: contas) {
			if(conta.getId() == idConta) {
				contaRepository.delete(conta);
			}
		}
		
		return "Deletado com sucesso";
	}
	
	public boolean verificaContaExistente(int idConta) {
		Conta conta = findById(idConta);
		
		if(conta != null) {
			return true;
		}
		return false;
		
	}
}




















