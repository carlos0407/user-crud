package com.restfulproject.crud.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restfulproject.crud.dto.ContactDto;
import com.restfulproject.crud.dto.RelationDto;
import com.restfulproject.crud.entities.Contact;
import com.restfulproject.crud.entities.UserContact;
import com.restfulproject.crud.repositories.ContactRepository;
import com.restfulproject.crud.responses.ResponseUserContact;
import com.restfulproject.crud.service.exceptions.ContactServiceException;

@Service
@Transactional
public class ContactService{
	

	private ContactRepository contactRepository;
	
	@Autowired
	private UserContactService userContactService;
	
	
	
	@Autowired
	public ContactService (ContactRepository contactRepository) {
		super();
		this.contactRepository = contactRepository;
	}
	
	public List<Contact> listarContatos(){
		return contactRepository.findAll();
	}

	public List<ContactDto> salvarContatos(RelationDto userContactDto) {
		
		List<ContactDto> contacts = userContactDto.getContacts();
		
		for(int i=0; i< userContactDto.getContacts().size(); i++) {
			Contact contact = new Contact();
			
			contact.setDdd(contacts.get(i).getDdd());
			contact.setPhone(contacts.get(i).getPhone());
			
			contactRepository.save(contact);
			
			contacts.get(i).setIdContact(contact.getId());
		}
		
		return contacts;
	}
	
	public Contact buscarContato(int idContact) {
		Contact contact = contactRepository.findById(idContact);
				
		if(contact == null) {
			throw new ContactServiceException("Contato não cadastrado");
		}
		
		return contact;
		
	}
	
	public List<ContactDto> salvarContatoPorIdUsuario(List<ContactDto> contactsDto) {
		
		for (ContactDto contactDto2 : contactsDto) {
			Contact contact = new Contact();
			contact.setDdd(contactDto2.getDdd());
			contact.setPhone(contactDto2.getPhone());
			contactRepository.saveAndFlush(contact);
			contactDto2.setIdContact(contact.getId());
		}
		return contactsDto;
	}
	
	public Contact atualizarContatoPorId(Integer idContact, ContactDto contactDto) {
		
		
		
		Contact contact = buscarContato(idContact);
		
		contact.setDdd(contactDto.getDdd());
		contact.setPhone(contactDto.getPhone());
		
		return contactRepository.saveAndFlush(contact);
	}
	
	public String deletarContato(Integer idContact) {
		
		List<UserContact> usersContacts = userContactService.listarUsuariosContatos();
		
		List<Contact> contacts = listarContatos();
		
		boolean flag = false;
		
		if(contacts.isEmpty())
			return "Lista de contatos vazia";
		
		for(int i = 0; i < usersContacts.size(); i++) {
			if(idContact == usersContacts.get(i).getContactId()) {
				userContactService.deletarUserContact(usersContacts.get(i));
			}
		}
		
		for(Contact contact: contacts) {
			if(contact.getId() == idContact) {
				contactRepository.delete(idContact);
				flag = true;
			}
		}
		if(flag)
			return "Contato deletado com sucesso!";
		else
			return "Contato não existente";
	}
	
	public List<Contact> buscarContatosPorIdUsuario(Integer idUser){
		
		ResponseUserContact response = userContactService.findUserContactById(idUser);
		
		return response.getContacts();
		
	}
	
	public boolean verifyPhone(List<ContactDto> contactsDto, int idUser) {
		if(idUser != 0) {
			List<Contact> contacts = userContactService.findUserContactById(idUser).getContacts();
			
			for(Contact contact: contacts) {
				for(int i=0; i<contactsDto.size(); i++) {
					if(contact.getDdd() == contactsDto.get(i).getDdd())
						if(contact.getPhone() == contactsDto.get(i).getPhone())
							return false;
				}
			}
		}
		
			
		for(int i = 0; i < contactsDto.size(); i++) {
			if(!validateDdd(contactsDto.get(i).getDdd()) || !validatePhone(contactsDto.get(i).getPhone()))
					return false;
			for(int j = i+1; j < contactsDto.size(); j++) {
				if(contactsDto.get(i) == contactsDto.get(j))
					return false;
			}
		}
		
		
		
		return true;
		
	}
	
	public boolean validatePhone(long phone) {
		String v = String.valueOf(phone);
		if(v.length() >= 8 && v.length() <= 9) {
			return true;
		} 
		return false;
	}
	
	public boolean validateDdd(Integer ddd) {
		String v = ddd.toString();
		if(v.length() == 2) {
			return true;
		}
		return false;
	}
	
	
	
	
}
