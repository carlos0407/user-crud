package com.restfulproject.crud.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restfulproject.crud.entities.ContaPagamento;
import com.restfulproject.crud.entities.Contact;
import com.restfulproject.crud.entities.User;
import com.restfulproject.crud.entities.UserContact;
import com.restfulproject.crud.repositories.UserContactRepository;
import com.restfulproject.crud.responses.ResponseUserContact;



@Service
@Transactional
public class UserContactService {
	
	@Autowired
	public UserContactRepository userContactRepository;
	
	@Autowired
	public UserService userService;
	
	@Autowired
	public ContactService contactService;
	
	@Autowired
	public ContaService contaService;
	
	@Autowired
	public ContaPagamentoService contaPagamentoService;
	
	public List<UserContact> listarUsuariosContatos(){
		return userContactRepository.findAll();
	}
	
	public String deletarUserContact(UserContact userContact) {
		userContactRepository.delete(userContact);
		return "Deletado com sucesso";
	}
	
	public UserContact salvar(Integer idUser, Integer idContact) {
		UserContact userContact = new UserContact();
		
		userContact.setUserId(idUser);
		userContact.setContactId(idContact);
		
		return userContactRepository.saveAndFlush(userContact);
	}
	
	public List<ResponseUserContact> encontrar(){
		List<UserContact> userContacts = listarUsuariosContatos();
		
		List<ResponseUserContact> response = new ArrayList<ResponseUserContact>();
		
		
		
		for(int i = 0; i < userContacts.size(); i++) {
			boolean flag = false;
			int idUser = userContacts.get(i).getUserId();
			
			List<Contact> contacts = new ArrayList<Contact>();
			ResponseUserContact responseUserContact = new ResponseUserContact();
			User user = new User();
			
			if(response.isEmpty()) {
				for(int k=0; k < userContacts.size(); k++) {
					if(idUser == userContacts.get(k).getUserId()) {
						Contact contact = new Contact();
						int idContact = userContacts.get(k).getContactId();
						contact = contactService.buscarContato(idContact);
						contacts.add(contact);
					}
				}
				user = userService.buscarUsuario(idUser);
				
				responseUserContact.setUser(user);
				responseUserContact.setContact(contacts);
				response.add(responseUserContact);
				
				
			} else {
				for(int j=0; j < response.size(); j++) {
					if(idUser == response.get(j).getUser().getId()){
						flag = true;
					}
				}
				if(flag) {
					continue;
				} else {
					for(int k=0; k < userContacts.size(); k++) {
						if(idUser == userContacts.get(k).getUserId()) {
							Contact contact = new Contact();
							int idContact = userContacts.get(k).getContactId();
							contact = contactService.buscarContato(idContact);
							contacts.add(contact);
						}
					}
					user = userService.buscarUsuario(idUser);
					
					responseUserContact.setUser(user);
					responseUserContact.setContact(contacts);
					response.add(responseUserContact);
				}
			}
		}
		return response;
	}
	
	public ResponseUserContact findUserContactById(int idUser) {
		
		List<UserContact> userContacts = listarUsuariosContatos();
		
		ResponseUserContact response = new ResponseUserContact();
		
		for(int i=0; i < userContacts.size(); i++) {
			
			List<Contact> contacts = new ArrayList<Contact>();
			User user = new User();
			
			for(int k=0; k < userContacts.size(); k++) {
				if(idUser == userContacts.get(k).getUserId()) {
					Contact contact = new Contact();
					int idContact = userContacts.get(k).getContactId();
					contact = contactService.buscarContato(idContact);
					contacts.add(contact);
				}
			}
			user = userService.buscarUsuario(idUser);
			
			response.setUser(user);
			response.setContact(contacts);
		}
		return response;
	}
	
	public String deleteUserContacts(Integer idUser) {
		List<UserContact> userContacts = listarUsuariosContatos();
		
		String nome = userService.buscarUsuario(idUser).getFirstname();
		
		for(int i = 0; i < userContacts.size(); i++) {
			if(idUser == userContacts.get(i).getUserId()) {
				Integer idContact = userContacts.get(i).getContactId();
				userContactRepository.delete(userContacts.get(i));
				contactService.deletarContato(idContact);
			}
		}
		
		userService.deletarUsuario(idUser);
		
		return "Usuário "+ nome +" e seus Contatos deletados com sucesso";
		
	}
	
	public String deleteUserContactsConta(Integer idUser) {
		
		List<UserContact> usersContacts = listarUsuariosContatos();
		
		for(UserContact userContact: usersContacts) {
			if(idUser == userContact.getUserId()) {
				Integer idContact = userContact.getContactId();
				userContactRepository.delete(userContact);
				contactService.deletarContato(idContact);
			}
		}
		
		if(contaPagamentoService.verificaContaExistenteIdUser(idUser)) {
			if(contaPagamentoService.verificaContaAdicionalOuTitular(idUser) == 1) {
				List<ContaPagamento> contas = contaPagamentoService.listarContaPagamento();
				contaPagamentoService.deletarContaPagamento(contaPagamentoService.findContaPagamentoByIdUser(contas,idUser));
				userService.deletarUsuario(idUser);
				int idConta = contaPagamentoService.findIdContaPeloIdUser(contas,idUser);
				
				List<ContaPagamento> contasPagamento = contaPagamentoService.listarContaPagamento();
				
				for(ContaPagamento contaPagamento: contasPagamento) {
					if(idConta == contaPagamento.getIdConta()) {
						contaPagamentoService.deletarContaPagamento(contaPagamento);
						if(contaPagamento.getTitularidade() == 2) { //2 = Adicional
							for(UserContact UC: usersContacts) {
								if(UC.getUserId() == contaPagamento.getIdUser()) {
									Integer idContact = UC.getContactId();
									userContactRepository.delete(UC);
									contactService.deletarContato(idContact);
								}
							}
						}
						userService.deletarUsuario(contaPagamento.getIdUser());
					}
				}
				
				return contaService.deleteConta(idConta);
				
			} else {
				List<ContaPagamento> contas = contaPagamentoService.listarContaPagamento();
				contaPagamentoService.deletarContaPagamento(contaPagamentoService.findContaPagamentoByIdUser(contas,idUser));
				String nome = userService.buscarUsuario(idUser).getFirstname();
				userService.deletarUsuario(idUser);
				
				return "Usuário "+ nome + " e seus contatos deletado com sucesso";
				
			}
		} else {
			
			return deleteUserContacts(idUser);
			
		}
	}
	
	
	
	

}
