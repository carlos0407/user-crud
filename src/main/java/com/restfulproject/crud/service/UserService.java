package com.restfulproject.crud.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restfulproject.crud.dto.RelationDto;
import com.restfulproject.crud.dto.UserDto;
import com.restfulproject.crud.entities.User;
import com.restfulproject.crud.repositories.UserRepository;
import com.restfulproject.crud.service.exceptions.UserServiceException;


@Service
@Transactional
public class UserService {


	public UserRepository userRepository;
	
	@Autowired
	public UserService (UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}
	
	public List<User> listarUsuarios(){
		return userRepository.findAll();
	}
	
	public User salvarUsuario(RelationDto userContactDto) {
		User user = new User();
		
		user.setFirstname(userContactDto.getFirstname());
		user.setLastname(userContactDto.getLastname());
		user.setAddress(userContactDto.getAddress());
		user.setCpf(userContactDto.getCpf());
		return userRepository.save(user);
	}
	
	public User buscarUsuario(int idUser) {
		User user = userRepository.findById(idUser);
		
		if(user == null) {
			throw new UserServiceException("Usuário não cadastrado");
		}
		
		return user;
	}
	
	public User alterarUsuarioPorId(int idUser, UserDto userDto) {
		User user = buscarUsuario(idUser);
		
		user.setFirstname(userDto.getFirstname());
		user.setLastname(userDto.getLastname());
		user.setAddress(userDto.getAddress());
		user.setCpf(userDto.getCpf());
		
		return userRepository.save(user);
	}
	
	public void deletarUsuario(Integer idUser) {
		userRepository.delete(idUser);
	}
	
	public boolean verificaUsuarioExistente(int idUser) {
		User user = buscarUsuario(idUser);
		
		if(user.getId() == idUser) {
			return true;
		} else
			return false;
	}
	
	public boolean verifyEqualCpf(String cpf) {
		
		List<User> users = listarUsuarios();
		
		for(User user: users) {
			if(user.getCpf().intern() == cpf.intern()) {
				return false;
			}	
		}
		return true;
	}
	
	public boolean validaCpf(String cpf) {
		char primeiroDigito, segundoDigito;
		int resultado = 0, j = 10, k = 0;
		
		
		
		if(cpf.length() != 11) {
			return false;
		} else{
			for(int i=0; i < cpf.length(); i++)
				if(!Character.isDigit(cpf.charAt(i))) {
					return false;
				}	
		}
		
		
		
		while(j > 1) {
			if(k < 10) {
				resultado += j * (cpf.charAt(k)-48);
				k++;
			}
			j--;
		}
		resultado %= 11;
		if(resultado <= 1) {
			primeiroDigito = '0';
		} else {
			resultado = (11 - resultado) + 48;
			primeiroDigito = (char) resultado;
		}
		if(primeiroDigito == cpf.charAt(k)) {
			j = 11;
			k = 0;
			resultado = 0;
			while(j > 1) {
				if(k < 11) {
					resultado += j * (cpf.charAt(k)-48);
					k++;
				}
				j--;
			}
			resultado %= 11;
			if(resultado <= 1) { 
				segundoDigito = '0';
			} else {
				resultado = (11 - resultado) + 48;
				segundoDigito = (char) resultado;
			}
			if(segundoDigito == cpf.charAt(k)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}
