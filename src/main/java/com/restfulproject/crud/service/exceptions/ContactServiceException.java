package com.restfulproject.crud.service.exceptions;


public class ContactServiceException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContactServiceException(String mensagem) {
		super(mensagem);
	}
	
	public ContactServiceException(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}
	

}
