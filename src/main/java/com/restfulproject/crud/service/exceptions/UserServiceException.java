package com.restfulproject.crud.service.exceptions;

public class UserServiceException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UserServiceException(String mensagem) {
		super(mensagem);
	}
	
	public UserServiceException(String mensagem, Throwable causa) {
		super(mensagem,causa);
	}

}
